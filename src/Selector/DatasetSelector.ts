/**
 * Bit&Black Theme Switcher.
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */

import { SelectorInterface } from "./SelectorInterface";

/**
 * The DatasetSelector handles the data attribute.
 */
class DatasetSelector implements SelectorInterface
{
    /**
     * The selectors name.
     */
    private readonly selector: string;

    /**
     * Constructor.
     * 
     * @param selectorName The data attribute name in CamelCase.
     */
    constructor(selectorName: string)
    {
        this.selector = selectorName;
    }

    /**
     * Returns the selectors name.
     */
    getSelector(): string 
    {
        return this.selector;
    }
}

export { DatasetSelector };