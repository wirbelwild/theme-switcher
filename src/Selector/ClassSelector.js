"use strict";
/**
 * Bit&Black Theme Switcher.
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */
exports.__esModule = true;
exports.ClassSelector = void 0;
/**
 * The ClassSelector handles the class attribute.
 */
var ClassSelector = /** @class */ (function () {
    /**
     * Constructor.
     *
     * @param selectorName The class name. Without whitespaces.
     */
    function ClassSelector(selectorName) {
        this.selector = selectorName;
    }
    /**
     * Returns the selectors name.
     */
    ClassSelector.prototype.getSelector = function () {
        return this.selector;
    };
    return ClassSelector;
}());
exports.ClassSelector = ClassSelector;
