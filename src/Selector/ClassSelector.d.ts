/**
 * Bit&Black Theme Switcher.
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */
import { SelectorInterface } from "./SelectorInterface";
/**
 * The ClassSelector handles the class attribute.
 */
declare class ClassSelector implements SelectorInterface {
    /**
     * The selectors name.
     */
    private readonly selector;
    /**
     * Constructor.
     *
     * @param selectorName The class name. Without whitespaces.
     */
    constructor(selectorName: string);
    /**
     * Returns the selectors name.
     */
    getSelector(): string;
}
export { ClassSelector };
