/**
 * Bit&Black Theme Switcher.
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */
/**
 * The SelectorInterface is for the selectors which appear in the `body` tag.
 */
interface SelectorInterface {
    /**
     * Returns the selector.
     */
    getSelector(): string;
}
export { SelectorInterface };
