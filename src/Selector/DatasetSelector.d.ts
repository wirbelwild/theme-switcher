/**
 * Bit&Black Theme Switcher.
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */
import { SelectorInterface } from "./SelectorInterface";
/**
 * The DatasetSelector handles the data attribute.
 */
declare class DatasetSelector implements SelectorInterface {
    /**
     * The selectors name.
     */
    private readonly selector;
    /**
     * Constructor.
     *
     * @param selectorName The data attribute name in CamelCase.
     */
    constructor(selectorName: string);
    /**
     * Returns the selectors name.
     */
    getSelector(): string;
}
export { DatasetSelector };
