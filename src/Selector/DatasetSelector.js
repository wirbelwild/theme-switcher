"use strict";
/**
 * Bit&Black Theme Switcher.
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */
exports.__esModule = true;
exports.DatasetSelector = void 0;
/**
 * The DatasetSelector handles the data attribute.
 */
var DatasetSelector = /** @class */ (function () {
    /**
     * Constructor.
     *
     * @param selectorName The data attribute name in CamelCase.
     */
    function DatasetSelector(selectorName) {
        this.selector = selectorName;
    }
    /**
     * Returns the selectors name.
     */
    DatasetSelector.prototype.getSelector = function () {
        return this.selector;
    };
    return DatasetSelector;
}());
exports.DatasetSelector = DatasetSelector;
