"use strict";
/*!
 * Bit&Black Theme Switcher.
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */
exports.__esModule = true;
exports.ThemeSwitcher = void 0;
var ClassSelector_1 = require("./Selector/ClassSelector");
var DatasetSelector_1 = require("./Selector/DatasetSelector");
/**
 * The Theme Switcher handles all states and the event too.
 */
var ThemeSwitcher = /** @class */ (function () {
    /**
     * Constructor.
     *
     * @param selector The name of the data selector for the body tag.
     * @param listener The listener.
     */
    function ThemeSwitcher(selector, listener) {
        var _this = this;
        /**
         * The current class modifier. This is needed to toggle the class names correctly.
         */
        this.currentClassModifier = null;
        this.validateValues(selector, listener);
        this.selector = selector;
        this.listener = listener;
        ThemeSwitcher.localStorageItemName = null !== ThemeSwitcher.localStorageItemName
            ? ThemeSwitcher.localStorageItemName
            : "theme-switcher";
        this.changeMode = this.changeMode.bind(this);
        var storedState = localStorage.getItem(ThemeSwitcher.localStorageItemName) || false;
        var darkModePreferred = window.matchMedia("(prefers-color-scheme: dark)").matches;
        var lightModePreferred = window.matchMedia("(prefers-color-scheme: light)").matches;
        this.listener.setEventCallBack(this.changeMode);
        var stateNew = this.listener.getState();
        var stateFound = null;
        /**
         * If the state was stored we're going to use that setting.
         * Otherwise, we'll have a look at the local settings and choose light of no setting was found.
         */
        if (false !== storedState) {
            stateFound = this.listener.getStateByName(storedState);
        }
        else if (darkModePreferred) {
            stateFound = ThemeSwitcher.preferredDarkModeState;
        }
        else if (lightModePreferred) {
            stateFound = ThemeSwitcher.preferredLightModeState;
        }
        /**
         * If ThemeSwitcher should sync with the device settings.
         */
        if (true === ThemeSwitcher.listenForDeviceChange) {
            this.deviceChangeListenerCallback(function (isDark) {
                _this.currentState = isDark
                    ? ThemeSwitcher.preferredDarkModeState
                    : ThemeSwitcher.preferredLightModeState;
                _this.updateDom();
            });
        }
        this.currentState = null !== stateFound ? stateFound : stateNew;
        this.updateDom();
        ThemeSwitcher.onInitCallback(this.currentState.getStateName());
    }
    /**
     * Changes the state.
     *
     * @param event
     */
    ThemeSwitcher.prototype.changeMode = function (event) {
        event.preventDefault();
        this.currentState = this.listener.getState();
        this.updateDom();
        localStorage.setItem(ThemeSwitcher.localStorageItemName, this.currentState.getStateName());
        ThemeSwitcher.onChangeCallback(this.currentState.getStateName());
    };
    /**
     * Sets the callback function when the class has been initialized.
     *
     * @param callback
     */
    ThemeSwitcher.setOnInitCallback = function (callback) {
        this.onInitCallback = callback;
    };
    /**
     * Sets the callback function when the state has been changed.
     *
     * @param callback
     */
    ThemeSwitcher.setOnChangeCallback = function (callback) {
        this.onChangeCallback = callback;
    };
    /**
     * Updates the DOM.
     */
    ThemeSwitcher.prototype.updateDom = function () {
        var body = document.querySelector("body");
        if (this.selector instanceof DatasetSelector_1.DatasetSelector) {
            body.dataset[this.selector.getSelector()] = this.currentState.getStateName();
            return;
        }
        if (this.selector instanceof ClassSelector_1.ClassSelector) {
            var classBlock = this.selector.getSelector();
            var classModifier = "".concat(this.selector.getSelector(), "--").concat(this.currentState.getStateName());
            body.classList.remove(this.currentClassModifier);
            body.classList.add(classBlock, classModifier);
            this.currentClassModifier = classModifier;
            return;
        }
    };
    /**
     * Sets the state which should be used when dark mode is preferred.
     *
     * @param state
     */
    ThemeSwitcher.setPreferredDarkModeState = function (state) {
        this.preferredDarkModeState = state;
    };
    /**
     * Sets the state which should be used when light mode is preferred.
     *
     * @param state
     */
    ThemeSwitcher.setPreferredLightModeState = function (state) {
        this.preferredLightModeState = state;
    };
    /**
     * Name of the local storage item.
     *
     * @param itemName
     */
    ThemeSwitcher.setLocalStorageItemName = function (itemName) {
        this.localStorageItemName = itemName;
    };
    /**
     * Validates the input and throws errors if something is wrong.
     *
     * @param selector
     * @param listener
     */
    ThemeSwitcher.prototype.validateValues = function (selector, listener) {
        if (selector instanceof DatasetSelector_1.DatasetSelector && selector.getSelector().includes("-")) {
            throw new Error("A dash is not allowed in a data attribute (\"".concat(selector.getSelector(), "\"). Use CamelCase instead, it will be transformed automatically."));
        }
        if (selector instanceof ClassSelector_1.ClassSelector) {
            if (selector.getSelector().includes(" ")) {
                throw new Error("A whitespace is not allowed in a class name (\"".concat(selector.getSelector(), "\"). Use dashes instead."));
            }
            var states = listener.getStates();
            var statesCount = states.length;
            for (var counter = 0; counter < statesCount; ++counter) {
                if (states[counter].getStateName().includes(" ")) {
                    throw new Error("A whitespace is not allowed in a class name (\"".concat(states[counter].getStateName(), "\"). Use dashes instead."));
                }
            }
        }
    };
    /**
     * A callback that will get executed when the device changes its preferences.
     *
     * @param callback
     * @private
     */
    ThemeSwitcher.prototype.deviceChangeListenerCallback = function (callback) {
        window
            .matchMedia("(prefers-color-scheme: dark)")
            .addEventListener("change", function (_a) {
            var isDark = _a.matches;
            callback(isDark);
        });
    };
    /**
     * Enables changing the State automatically when the device changes its setting.
     */
    ThemeSwitcher.listenForDeviceChangeEnabled = function () {
        if (null === ThemeSwitcher.preferredDarkModeState || null === ThemeSwitcher.preferredLightModeState) {
            throw new Error("Cannot enable listening for device changes without states. "
                + "Please define them at first by calling \"ThemeSwitcher.setPreferredDarkModeState\" and "
                + "\"ThemeSwitcher.setPreferredLightModeState\"");
        }
        this.listenForDeviceChange = true;
    };
    /**
     * The callback function for after this class has been initializes.
     */
    ThemeSwitcher.onInitCallback = function () { };
    /**
     * The callback function for when the state has been changed.
     */
    ThemeSwitcher.onChangeCallback = function () { };
    /**
     * The state which should be used when dark mode is preferred.
     */
    ThemeSwitcher.preferredDarkModeState = null;
    /**
     * The state which should be used when dark mode is preferred.
     */
    ThemeSwitcher.preferredLightModeState = null;
    /**
     * Name of the local storage item.
     */
    ThemeSwitcher.localStorageItemName = null;
    /**
     * If the State should change automatically when the device changes its setting.
     */
    ThemeSwitcher.listenForDeviceChange = false;
    return ThemeSwitcher;
}());
exports.ThemeSwitcher = ThemeSwitcher;
