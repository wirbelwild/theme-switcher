/*!
 * Bit&Black Theme Switcher.
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */

import { ClassSelector } from "./Selector/ClassSelector";
import { DatasetSelector } from "./Selector/DatasetSelector";
import { ListenerInterface } from "./Listener/ListenerInterface";
import { SelectorInterface } from "./Selector/SelectorInterface";
import { State } from "./State";

/**
 * The Theme Switcher handles all states and the event too.
 */
class ThemeSwitcher 
{
    /**
     * The selector.
     */
    private readonly selector: SelectorInterface;

    /**
     * The listener.
     */
    private readonly listener: ListenerInterface;

    /**
     * The callback function for after this class has been initializes.
     */
    private static onInitCallback: (stateName: string) => void = () => {};

    /**
     * The callback function for when the state has been changed.
     */
    private static onChangeCallback: (stateName: string) => void = () => {};

    /**
     * The state which should be used when dark mode is preferred.
     */
    private static preferredDarkModeState: State | null = null;

    /**
     * The state which should be used when dark mode is preferred.
     */
    private static preferredLightModeState: State | null = null;

    /**
     * The current state.
     */
    private currentState: State;

    /**
     * The current class modifier. This is needed to toggle the class names correctly.
     */
    private currentClassModifier: string | null = null;

    /**
     * Name of the local storage item.
     */
    private static localStorageItemName: string | null = null;

    /**
     * If the State should change automatically when the device changes its setting.
     */
    private static listenForDeviceChange: boolean = false;

    /**
     * Constructor.
     *
     * @param selector The name of the data selector for the body tag.
     * @param listener The listener.
     */
    constructor(selector: SelectorInterface, listener: ListenerInterface) 
    {
        this.validateValues(selector, listener);

        this.selector = selector;
        this.listener = listener;

        ThemeSwitcher.localStorageItemName = null !== ThemeSwitcher.localStorageItemName 
            ? ThemeSwitcher.localStorageItemName 
            : "theme-switcher"
        ;
        
        this.changeMode = this.changeMode.bind(this);

        const storedState: string | boolean = localStorage.getItem(ThemeSwitcher.localStorageItemName) || false;
        const darkModePreferred = window.matchMedia("(prefers-color-scheme: dark)").matches;
        const lightModePreferred = window.matchMedia("(prefers-color-scheme: light)").matches;

        this.listener.setEventCallBack(this.changeMode);
        
        const stateNew = this.listener.getState();
        let stateFound = null;

        /**
         * If the state was stored we're going to use that setting.
         * Otherwise, we'll have a look at the local settings and choose light of no setting was found.
         */
        if (false !== storedState) {
            stateFound = this.listener.getStateByName(storedState);
        } else if (darkModePreferred) {
            stateFound = ThemeSwitcher.preferredDarkModeState;
        } else if (lightModePreferred) {
            stateFound = ThemeSwitcher.preferredLightModeState;
        }

        /**
         * If ThemeSwitcher should sync with the device settings.
         */
        if (true === ThemeSwitcher.listenForDeviceChange) {
            this.deviceChangeListenerCallback((isDark) => {
                this.currentState = isDark
                    ? ThemeSwitcher.preferredDarkModeState
                    : ThemeSwitcher.preferredLightModeState
                ;
                this.updateDom();
            });
        }

        this.currentState = null !== stateFound ? stateFound : stateNew;

        this.updateDom();
        ThemeSwitcher.onInitCallback(this.currentState.getStateName());
    }

    /**
     * Changes the state.
     *
     * @param event
     */
    changeMode(event): void 
    {
        event.preventDefault();
        this.currentState = this.listener.getState();
        this.updateDom();
        localStorage.setItem(ThemeSwitcher.localStorageItemName, this.currentState.getStateName());
        ThemeSwitcher.onChangeCallback(this.currentState.getStateName());
    }

    /**
     * Sets the callback function when the class has been initialized.
     *
     * @param callback
     */
    static setOnInitCallback(callback): void 
    {
        this.onInitCallback = callback;
    }

    /**
     * Sets the callback function when the state has been changed.
     *
     * @param callback
     */
    static setOnChangeCallback(callback): void
    {
        this.onChangeCallback = callback;
    }

    /**
     * Updates the DOM.
     */
    updateDom(): void
    {
        const body = document.querySelector("body");

        if (this.selector instanceof DatasetSelector) {
            body.dataset[this.selector.getSelector()] = this.currentState.getStateName();
            return;
        } 
        
        if (this.selector instanceof ClassSelector) {
            const classBlock = this.selector.getSelector();
            const classModifier = `${this.selector.getSelector()}--${this.currentState.getStateName()}`;
            body.classList.remove(this.currentClassModifier);
            body.classList.add(classBlock, classModifier);
            this.currentClassModifier = classModifier;
            return;
        }
    }

    /**
     * Sets the state which should be used when dark mode is preferred.
     * 
     * @param state
     */
    static setPreferredDarkModeState(state: State): void 
    {
        this.preferredDarkModeState = state;
    }

    /**
     * Sets the state which should be used when light mode is preferred.
     * 
     * @param state
     */
    static setPreferredLightModeState(state: State): void 
    {
        this.preferredLightModeState = state;
    }

    /**
     * Name of the local storage item.
     * 
     * @param itemName
     */
    static setLocalStorageItemName(itemName: string): void 
    {
        this.localStorageItemName = itemName;    
    }

    /**
     * Validates the input and throws errors if something is wrong.
     * 
     * @param selector
     * @param listener
     */
    private validateValues(selector: SelectorInterface, listener: ListenerInterface): void 
    {
        if (selector instanceof DatasetSelector && selector.getSelector().includes("-")) {
            throw new Error(`A dash is not allowed in a data attribute ("${selector.getSelector()}"). Use CamelCase instead, it will be transformed automatically.`);
        }

        if (selector instanceof ClassSelector) {

            if (selector.getSelector().includes(" ")) {
                throw new Error(`A whitespace is not allowed in a class name ("${selector.getSelector()}"). Use dashes instead.`);
            }

            const states = listener.getStates();
            const statesCount = states.length;

            for (let counter = 0; counter < statesCount; ++counter) {
                if (states[counter].getStateName().includes(" ")) {
                    throw new Error(`A whitespace is not allowed in a class name ("${states[counter].getStateName()}"). Use dashes instead.`);
                }
            }
        }    
    }

    /**
     * A callback that will get executed when the device changes its preferences.
     *
     * @param callback
     * @private
     */
    private deviceChangeListenerCallback(callback): void
    {
        window
            .matchMedia("(prefers-color-scheme: dark)")
            .addEventListener("change", ({matches: isDark}) => {
                callback(isDark);
            })
        ;
    }

    /**
     * Enables changing the State automatically when the device changes its setting.
     */
    static listenForDeviceChangeEnabled(): void
    {
        if (null === ThemeSwitcher.preferredDarkModeState || null === ThemeSwitcher.preferredLightModeState) {
            throw new Error(`Cannot enable listening for device changes without states. `
                + `Please define them at first by calling "ThemeSwitcher.setPreferredDarkModeState" and `
                + `"ThemeSwitcher.setPreferredLightModeState"`
            );
        }

        this.listenForDeviceChange = true;
    }
}

export { ThemeSwitcher };