/*!
 * Bit&Black Theme Switcher.
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */
import { ListenerInterface } from "./Listener/ListenerInterface";
import { SelectorInterface } from "./Selector/SelectorInterface";
import { State } from "./State";
/**
 * The Theme Switcher handles all states and the event too.
 */
declare class ThemeSwitcher {
    /**
     * The selector.
     */
    private readonly selector;
    /**
     * The listener.
     */
    private readonly listener;
    /**
     * The callback function for after this class has been initializes.
     */
    private static onInitCallback;
    /**
     * The callback function for when the state has been changed.
     */
    private static onChangeCallback;
    /**
     * The state which should be used when dark mode is preferred.
     */
    private static preferredDarkModeState;
    /**
     * The state which should be used when dark mode is preferred.
     */
    private static preferredLightModeState;
    /**
     * The current state.
     */
    private currentState;
    /**
     * The current class modifier. This is needed to toggle the class names correctly.
     */
    private currentClassModifier;
    /**
     * Name of the local storage item.
     */
    private static localStorageItemName;
    /**
     * If the State should change automatically when the device changes its setting.
     */
    private static listenForDeviceChange;
    /**
     * Constructor.
     *
     * @param selector The name of the data selector for the body tag.
     * @param listener The listener.
     */
    constructor(selector: SelectorInterface, listener: ListenerInterface);
    /**
     * Changes the state.
     *
     * @param event
     */
    changeMode(event: any): void;
    /**
     * Sets the callback function when the class has been initialized.
     *
     * @param callback
     */
    static setOnInitCallback(callback: any): void;
    /**
     * Sets the callback function when the state has been changed.
     *
     * @param callback
     */
    static setOnChangeCallback(callback: any): void;
    /**
     * Updates the DOM.
     */
    updateDom(): void;
    /**
     * Sets the state which should be used when dark mode is preferred.
     *
     * @param state
     */
    static setPreferredDarkModeState(state: State): void;
    /**
     * Sets the state which should be used when light mode is preferred.
     *
     * @param state
     */
    static setPreferredLightModeState(state: State): void;
    /**
     * Name of the local storage item.
     *
     * @param itemName
     */
    static setLocalStorageItemName(itemName: string): void;
    /**
     * Validates the input and throws errors if something is wrong.
     *
     * @param selector
     * @param listener
     */
    private validateValues;
    /**
     * A callback that will get executed when the device changes its preferences.
     *
     * @param callback
     * @private
     */
    private deviceChangeListenerCallback;
    /**
     * Enables changing the State automatically when the device changes its setting.
     */
    static listenForDeviceChangeEnabled(): void;
}
export { ThemeSwitcher };
