"use strict";
/**
 * Bit&Black Theme Switcher.
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */
var __spreadArray = (this && this.__spreadArray) || function (to, from, pack) {
    if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
        if (ar || !(i in from)) {
            if (!ar) ar = Array.prototype.slice.call(from, 0, i);
            ar[i] = from[i];
        }
    }
    return to.concat(ar || Array.prototype.slice.call(from));
};
exports.__esModule = true;
exports.ChangeListener = void 0;
/**
 * The ChangeListener is made for handling a form with radio buttons or a select field.
 */
var ChangeListener = /** @class */ (function () {
    /**
     * Constructor.
     *
     * @param element
     * @param initialState
     * @param secondState
     * @param additionalStates
     */
    function ChangeListener(element, initialState, secondState) {
        var _a;
        var additionalStates = [];
        for (var _i = 3; _i < arguments.length; _i++) {
            additionalStates[_i - 3] = arguments[_i];
        }
        /**
         * All states.
         */
        this.states = [];
        this.element = element;
        (_a = this.states).push.apply(_a, __spreadArray([initialState, secondState], additionalStates, false));
    }
    /**
     * Returns the HTML element.
     */
    ChangeListener.prototype.getElement = function () {
        return this.element;
    };
    /**
     * Returns the current state.
     */
    ChangeListener.prototype.getState = function () {
        return this.currentState;
    };
    /**
     * Returns a state by its name.
     *
     * @param storedState
     */
    ChangeListener.prototype.getStateByName = function (storedState) {
        for (var counter = 0; counter < this.getStates().length; ++counter) {
            var state = this.getStates()[counter];
            if (state.getStateName() === storedState) {
                return state;
            }
        }
        return null;
    };
    /**
     * Returns all states.
     */
    ChangeListener.prototype.getStates = function () {
        return this.states;
    };
    /**
     * Sets the callback for the event.
     *
     * @param callback
     */
    ChangeListener.prototype.setEventCallBack = function (callback) {
        var _this = this;
        this.getElement().addEventListener("change", function (event) {
            var stateName = event.target.value;
            var stateNew = _this.getStateByName(stateName);
            if (null === stateNew) {
                return;
            }
            _this.currentState = stateNew;
            callback(event);
        });
    };
    return ChangeListener;
}());
exports.ChangeListener = ChangeListener;
