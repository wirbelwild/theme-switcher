/**
 * Bit&Black Theme Switcher.
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */
import { State } from "../State";
/**
 * The ListenerInterface is for all listeners. They handle the states.
 */
interface ListenerInterface {
    /**
     * Returns the HTML element.
     */
    getElement(): HTMLElement;
    /**
     * Returns all states.
     */
    getStates(): State[];
    /**
     * Returns the current state.
     */
    getState(): State;
    /**
     * Returns a state by its name.
     *
     * @param storedState
     */
    getStateByName(storedState: string): State | null;
    /**
     * Sets the callback for the event.
     *
     * @param callback
     */
    setEventCallBack(callback: (event: any) => void): void;
}
export { ListenerInterface };
