/**
 * Bit&Black Theme Switcher.
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */

import { ListenerInterface } from "./ListenerInterface";
import { State } from "../State";

/**
 * The ChangeListener is made for handling a form with radio buttons or a select field.
 */
class ChangeListener implements ListenerInterface
{
    /**
     * The HTML element.
     */
    private readonly element: HTMLFormElement;

    /**
     * All states.
     */
    private states: State[] = [];

    /**
     * The current selected state.
     */
    private currentState: State;
    
    /**
     * Constructor.
     *
     * @param element
     * @param initialState
     * @param secondState
     * @param additionalStates
     */
    constructor(element: HTMLFormElement, initialState: State, secondState: State, ...additionalStates: State[]) 
    {
        this.element = element;
        this.states.push(initialState, secondState, ...additionalStates);
    }

    /**
     * Returns the HTML element.
     */
    getElement(): HTMLElement 
    {
        return this.element;
    }

    /**
     * Returns the current state.
     */
    getState(): State 
    {
        return this.currentState;
    }

    /**
     * Returns a state by its name.
     * 
     * @param storedState
     */
    getStateByName(storedState: string): State | null
    {
        for (let counter = 0; counter < this.getStates().length; ++counter) {
            const state = this.getStates()[counter];
            if (state.getStateName() === storedState) {
                return state;
            }
        }

        return null;
    }

    /**
     * Returns all states.
     */
    getStates(): State[] 
    {
        return this.states;
    }

    /**
     * Sets the callback for the event.
     * 
     * @param callback
     */
    setEventCallBack(callback: (event) => void): void 
    {
        this.getElement().addEventListener("change", (event) => {
            const stateName = (event.target as HTMLFormElement).value;
            const stateNew = this.getStateByName(stateName);
            
            if (null === stateNew) {
                return;
            }
            
            this.currentState = stateNew;
            callback(event);
        });
    }
}

export { ChangeListener };