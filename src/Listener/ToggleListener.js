"use strict";
/**
 * Bit&Black Theme Switcher.
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */
var __spreadArray = (this && this.__spreadArray) || function (to, from, pack) {
    if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
        if (ar || !(i in from)) {
            if (!ar) ar = Array.prototype.slice.call(from, 0, i);
            ar[i] = from[i];
        }
    }
    return to.concat(ar || Array.prototype.slice.call(from));
};
exports.__esModule = true;
exports.ToggleListener = void 0;
/**
 * The ToggleListener is made for handling a single element which toggles the states.
 */
var ToggleListener = /** @class */ (function () {
    /**
     * Constructor.
     *
     * @param element
     * @param initialState
     * @param secondState
     * @param additionalStates
     */
    function ToggleListener(element, initialState, secondState) {
        var _a;
        var additionalStates = [];
        for (var _i = 3; _i < arguments.length; _i++) {
            additionalStates[_i - 3] = arguments[_i];
        }
        /**
         * All states.
         */
        this.states = [];
        this.element = element;
        (_a = this.states).push.apply(_a, __spreadArray([initialState, secondState], additionalStates, false));
        this.stateCounter = 1;
    }
    /**
     * Returns the HTML element.
     */
    ToggleListener.prototype.getElement = function () {
        return this.element;
    };
    /**
     * Returns the current state.
     */
    ToggleListener.prototype.getState = function () {
        var length = this.states.length;
        ++this.stateCounter;
        if (this.stateCounter === length) {
            this.stateCounter = 0;
        }
        return this.states[this.stateCounter];
    };
    /**
     * Returns a state by its name.
     *
     * @param storedState
     */
    ToggleListener.prototype.getStateByName = function (storedState) {
        for (var counter = 0; counter < this.states.length; ++counter) {
            var state = this.states[counter];
            if (state.getStateName() === storedState) {
                this.stateCounter = counter;
                return state;
            }
        }
        return null;
    };
    /**
     * Returns all states.
     */
    ToggleListener.prototype.getStates = function () {
        return this.states;
    };
    /**
     * Sets the callback for the event.
     *
     * @param callback
     */
    ToggleListener.prototype.setEventCallBack = function (callback) {
        this.getElement().addEventListener("click", callback);
    };
    return ToggleListener;
}());
exports.ToggleListener = ToggleListener;
