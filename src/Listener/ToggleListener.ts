/**
 * Bit&Black Theme Switcher.
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */

import { ListenerInterface } from "./ListenerInterface";
import { State } from "../State";

/**
 * The ToggleListener is made for handling a single element which toggles the states.
 */
class ToggleListener implements ListenerInterface
{
    /**
     * The HTML element.
     */
    private readonly element: HTMLElement;

    /**
     * All states.
     */
    private states: State[] = [];

    /**
     * The current state number.
     */
    private stateCounter: number;
    
    /**
     * Constructor.
     *
     * @param element
     * @param initialState
     * @param secondState
     * @param additionalStates
     */
    constructor(element: HTMLElement, initialState: State, secondState: State, ...additionalStates: State[]) 
    {
        this.element = element;
        this.states.push(initialState, secondState, ...additionalStates);
        this.stateCounter = 1;
    }

    /**
     * Returns the HTML element.
     */
    getElement(): HTMLElement 
    {
        return this.element;
    }

    /**
     * Returns the current state.
     */
    getState(): State 
    {
        const length = this.states.length;
        ++this.stateCounter;
        
        if (this.stateCounter === length) {
            this.stateCounter = 0;
        }
        
        return this.states[this.stateCounter];
    }

    /**
     * Returns a state by its name.
     * 
     * @param storedState
     */
    getStateByName(storedState: string): State | null
    {
        for (let counter = 0; counter < this.states.length; ++counter) {
            const state = this.states[counter];
            if (state.getStateName() === storedState) {
                this.stateCounter = counter;
                return state;
            }
        }

        return null;
    }

    /**
     * Returns all states.
     */
    getStates(): State[] 
    {
        return this.states;
    }
    
    /**
     * Sets the callback for the event.
     * 
     * @param callback
     */
    setEventCallBack(callback: (event) => void): void
    {
        this.getElement().addEventListener("click", callback);
    }
}

export { ToggleListener };