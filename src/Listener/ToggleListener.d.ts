/**
 * Bit&Black Theme Switcher.
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */
import { ListenerInterface } from "./ListenerInterface";
import { State } from "../State";
/**
 * The ToggleListener is made for handling a single element which toggles the states.
 */
declare class ToggleListener implements ListenerInterface {
    /**
     * The HTML element.
     */
    private readonly element;
    /**
     * All states.
     */
    private states;
    /**
     * The current state number.
     */
    private stateCounter;
    /**
     * Constructor.
     *
     * @param element
     * @param initialState
     * @param secondState
     * @param additionalStates
     */
    constructor(element: HTMLElement, initialState: State, secondState: State, ...additionalStates: State[]);
    /**
     * Returns the HTML element.
     */
    getElement(): HTMLElement;
    /**
     * Returns the current state.
     */
    getState(): State;
    /**
     * Returns a state by its name.
     *
     * @param storedState
     */
    getStateByName(storedState: string): State | null;
    /**
     * Returns all states.
     */
    getStates(): State[];
    /**
     * Sets the callback for the event.
     *
     * @param callback
     */
    setEventCallBack(callback: (event: any) => void): void;
}
export { ToggleListener };
