/**
 * Bit&Black Theme Switcher.
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */

/**
 * The state, for example `light` or `dark`.
 */
class State 
{
    /**
     * The states name.
     */
    private readonly stateName: string;

    /**
     * Constructor.
     * 
     * @param stateName The state name, for example `light` or `dark`.
     */
    constructor(stateName: string) 
    {
        this.stateName = stateName;
    }

    /**
     * Returns the states name.
     */
    getStateName(): string 
    {
        return this.stateName;
    }
}

export { State };