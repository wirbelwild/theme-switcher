/**
 * Bit&Black Theme Switcher.
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */
/**
 * The state, for example `light` or `dark`.
 */
declare class State {
    /**
     * The states name.
     */
    private readonly stateName;
    /**
     * Constructor.
     *
     * @param stateName The state name, for example `light` or `dark`.
     */
    constructor(stateName: string);
    /**
     * Returns the states name.
     */
    getStateName(): string;
}
export { State };
