"use strict";
/**
 * Bit&Black Theme Switcher.
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */
exports.__esModule = true;
exports.State = void 0;
/**
 * The state, for example `light` or `dark`.
 */
var State = /** @class */ (function () {
    /**
     * Constructor.
     *
     * @param stateName The state name, for example `light` or `dark`.
     */
    function State(stateName) {
        this.stateName = stateName;
    }
    /**
     * Returns the states name.
     */
    State.prototype.getStateName = function () {
        return this.stateName;
    };
    return State;
}());
exports.State = State;
