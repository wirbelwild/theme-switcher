const Encore = require("@symfony/webpack-encore");

Encore
    .setOutputPath("example/build/")
    .setPublicPath("/")
    .addEntry("example", "./example/assets/example.js")
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(false)
    .enableVersioning(false)
    .disableSingleRuntimeChunk()
    .enableTypeScriptLoader()
;

const config = Encore.getWebpackConfig();

module.exports = config;
