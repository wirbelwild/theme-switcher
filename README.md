[![Codacy Badge](https://app.codacy.com/project/badge/Grade/70c768af5da54c1e9401d6adb1f39a90)](https://www.codacy.com/bb/wirbelwild/theme-switcher/dashboard)
[![npm version](https://badge.fury.io/js/bitandblack-theme-switcher.svg)](https://badge.fury.io/js/bitandblack-theme-switcher)

# Theme Switcher

Handles and stores states in the local storage. This can be used to handle different color modes.

## Installation 

This library is made for the use with [Node](https://www.npmjs.com/package/bitandblack-theme-switcher). Add it to your project by running `$ yarn add bitandblack-theme-switcher` or `$ npm install bitandblack-theme-switcher`.

## Usage

### Initialize

The theme switcher needs to be initialized with two objects: 

1.  A selector's instance: This can be the `ClassSelector` or the `DatasetSelector`.
    *   The `ClassSelector` will add the states an `BEM` style to the `body` class list. 
    *   The `DatasetSelector` will add a data attribute to the `body`.
    
2.  A listener's instance. This can be the `ChangeListener` or the `ToggleListener`.
    *   The `ChangeListener` is made to handle `radio` buttons or `select` fields.
    *   The `ToggleListener` is made to handle a single element which toggles the states. 
    
    The listener needs to know the HTML element to which it shall be added.
    
All together, this looks like that:

````javascript
import { ChangeListener, State, ThemeSwitcher } from "bitandblack-theme-switcher";

const themeSwitcher = new ThemeSwitcher(
    new ClassSelector("theme-class"),
    new ChangeListener(
        document.querySelector("#switcher"),
        new State("dark"),
        new State("light")
    )
);
````

This initializes the theme switcher, adds the class `theme-class` to the `body` and registers two states, `dark` and `light`. The active state `dark` will be added as a modifier class to the `body` too: `theme-class--dark`. 

### Usage in CSS

To use the example above in your CSS, add styles like that: 

````css
body.theme-class.theme-class--dark {
    color: #fff;
    background-color: #000;
}

body.theme-class.theme-class--light {
    color: #000;
    background-color: #fff;
}
````

For sure, it's a lot more flexible with SASS/SCSS and with variables holding the color values:

````scss
:root {
    --color-dark: #000;
    --color-light: #fff;
    --color-foreground: var(--color-dark);
    --color-background: var(--color-light);
}

.theme-class {
    &#{&}--light {
        --color-foreground: var(--color-dark);
        --color-background: var(--color-light);
    }
    
    &#{&}--dark {
        --color-foreground: var(--color-light);
        --color-background: var(--color-dark);
    }
}

body {
    color: var(--color-foreground);
    background-color: var(--color-background);
}
````

### Callbacks 

There are two callbacks possible which can be defined statically: one to get the initial state and one to get the updated/current state:

````javascript
import { ThemeSwitcher } from "bitandblack-theme-switcher";

ThemeSwitcher.setOnInitCallback((stateName) => {
    console.log(`Initialized with state "${stateName}".`);
});

ThemeSwitcher.setOnChangeCallback((stateName) => {
    console.log(`Updated with state "${stateName}".`);
});
````

### Preferred color scheme

It's possible to make use of the users preferred color scheme. Add the state you want to use for that scheme and pass it statically before initializing the theme switcher. If one of those schemes has been detected, the appropriate state is getting used at first.

````javascript
import { ThemeSwitcher } from "bitandblack-theme-switcher";

const dark = new State("dark");
const light = new State("light");

ThemeSwitcher.setPreferredDarkModeState(dark);
ThemeSwitcher.setPreferredLightModeState(light);
````

### Local storage

The theme switcher stores the state in the local storage. The default name is `theme-switcher`, but you can customize it. This method has to be called also statically before initializing the object.

```javascript
import { ThemeSwitcher } from "bitandblack-theme-switcher";

ThemeSwitcher.setLocalStorageItemName("custom-name");
```

## Example

There's an example in the `example` folder. It needs some build files which need to be created before. To do so, run `$ yarn build-example`. After that, open `example/example.html` in your browser.

## Help 

If you have any questions, feel free to contact us under `hello@bitandblack.com`.

Further information about Bit&Black can be found under [www.bitandblack.com](https://www.bitandblack.com).
