"use strict";
/*!
 * Bit&Black Theme Switcher.
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
exports.__esModule = true;
exports.ThemeSwitcher = exports.State = exports.DatasetSelector = exports.ClassSelector = exports.ToggleListener = exports.ChangeListener = void 0;
var ChangeListener_1 = require("./src/Listener/ChangeListener");
__createBinding(exports, ChangeListener_1, "ChangeListener");
var ToggleListener_1 = require("./src/Listener/ToggleListener");
__createBinding(exports, ToggleListener_1, "ToggleListener");
var ClassSelector_1 = require("./src/Selector/ClassSelector");
__createBinding(exports, ClassSelector_1, "ClassSelector");
var DatasetSelector_1 = require("./src/Selector/DatasetSelector");
__createBinding(exports, DatasetSelector_1, "DatasetSelector");
var State_1 = require("./src/State");
__createBinding(exports, State_1, "State");
var ThemeSwitcher_1 = require("./src/ThemeSwitcher");
__createBinding(exports, ThemeSwitcher_1, "ThemeSwitcher");
