import { ClassSelector, ChangeListener, State, ThemeSwitcher } from "../../index";

/**
 * Those are the two possible states:
 */
const stateDark = new State("dark");
const stateLight = new State("light");

/**
 * Those two calls help the ThemeSwitcher to understand how we want to treat those States.
 * Now the State can be chosen depending on the devices setting.
 */
ThemeSwitcher.setPreferredDarkModeState(stateDark);
ThemeSwitcher.setPreferredLightModeState(stateLight);

/**
 * This option allows as to change the State when the device changes its preferences.
 */
ThemeSwitcher.listenForDeviceChangeEnabled();

/**
 * Here we have two callbacks to handle the current state:
 */
ThemeSwitcher.setOnInitCallback((stateName) => {
    console.log(`Initialized with state "${stateName}".`);
});
ThemeSwitcher.setOnChangeCallback((stateName) => {
    console.log(`Updated with state "${stateName}".`);
});

/**
 * And now as we set up everything properly we can initialize the ThemeSwitcher.
 */
new ThemeSwitcher(
    new ClassSelector("theme-class"),
    new ChangeListener(
        document.querySelector("#switcher"),
        stateDark,
        stateLight
    )
);
